package server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import server.domain.Order;


public interface OrderRepository extends JpaRepository<Order,Long> {
}
package server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import server.domain.Buffet;


public interface BuffetRepository extends JpaRepository<Buffet,Long> {
}
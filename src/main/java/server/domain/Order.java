package server.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "RSOI2_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_id")
    private Long id;

    @Column(name="total_price")
    private Integer totalPrice;

    @ManyToMany
    @JoinTable(name="order_item",
            joinColumns = @JoinColumn(name="order_id"),
            inverseJoinColumns = @JoinColumn(name="item_id"))
    private List<Item> items;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buffet_id")
    private Buffet buffet;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<Item> getItems() {
        return items;
    }

    public Buffet getBuffet() {
        return buffet;
    }

    public User getUser() {
        return user;
    }

    public Order setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Order setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public Order setBuffet(Buffet buffet) {
        this.buffet = buffet;
        return this;
    }

    public Order setUser(User user) {
        this.user = user;
        return this;
    }

    public Order addItem(Item item) {
        this.items.add(item);
        return this;
    }

    public Order delItem(Item item) {
        this.items.remove(item);
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffet)
                .add("user", user)
                .toString();
    }

    public Order(Integer totalPrice, List<Item> items) {
        this.totalPrice = totalPrice;
        this.items = items;
    }
}

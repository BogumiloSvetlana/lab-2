package server.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "RSOI2_buffet")
public class Buffet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="buffet_id")
    private  Long id;

    @Column
    private String name;

    @Column
    private String location;

    @OneToMany(mappedBy = "buffet")
    private List<Order> orders;

    @ManyToMany
    @JoinTable(name="buffet_item",
            joinColumns = @JoinColumn(name="buffet_id"),
            inverseJoinColumns = @JoinColumn(name="item_id"))
    private List<Item> items;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<Item> getItems() {
        return items;
    }

    public Buffet setName(String name) {
        this.name = name;
        return this;
    }

    public Buffet setLocation(String location) {
        this.location = location;
        return this;
    }

    public Buffet setOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    public Buffet setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public Buffet addItem(Item item) {
        this.items.add(item);
        return this;
    }

    public Buffet delItem(Item item) {
        this.items.remove(item);
        return this;
    }

    public  Buffet addOrder(Order order) {
        this.orders.add(order);
        return this;
    }

    public  Buffet delOrder(Order order) {
        this.orders.remove(order);
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("location", location)
                .add("orders", orders)
                .add("items", items)
                .toString();
    }

    public Buffet(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public Buffet() {
        this.name = "";
        this.location = "";
    }

}

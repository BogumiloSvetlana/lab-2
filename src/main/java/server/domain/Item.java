package server.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "RSOI2_item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="item_id")
    private  Long id;

    @Column
    private String name;

    @Column
    private Integer price;

    @ManyToMany
    @JoinTable(name="order_item",
            joinColumns = @JoinColumn(name="item_id"),
            inverseJoinColumns = @JoinColumn(name="order_id"))
    private List<Order> orders;

    @ManyToMany
    @JoinTable(name="buffet_item",
            joinColumns = @JoinColumn(name="item_id"),
            inverseJoinColumns = @JoinColumn(name="buffet_id"))
    private List<Buffet> buffets;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<Buffet> getBuffets() {
        return buffets;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public Item setPrice(int price) {
        this.price = price;
        return this;
    }

    public Item setOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    public Item setBuffets(List<Buffet> buffets) {
        this.buffets = buffets;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("price", price)
                .add("orders", orders)
                .add("buffets", buffets)
                .toString();
    }

    public Item(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public Item() {
        this.name = "";
        this.price = 0;
    }

}

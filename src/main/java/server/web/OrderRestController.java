package server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.domain.Order;
import server.service.OrderService;
import server.service.UserService;
import server.web.model.Requests.OrderRequest;
import server.web.model.Response.OrderResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/order")
public class OrderRestController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable Long id) {
        return new OrderResponse(orderService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<OrderResponse> getAll() {
        return orderService.getAll()
                .stream()
                .map(OrderResponse::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrder(HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        OrderRequest orderRequest = fullOrderRequest.getBody();
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Order order = orderService.save(orderRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return order.getId();
        } else {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Long updateOrder(@PathVariable Long id, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        OrderRequest orderRequest = fullOrderRequest.getBody();
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Order order = orderService.update(id,orderRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return order.getId();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable Long id, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            orderService.delete(id);
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/Item/{itemId}", method = RequestMethod.POST)
    public void addItemToOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedOrderId = orderService.addItemToOrder(orderId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/order/"+updatedOrderId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Item/{itemId}", method = RequestMethod.DELETE)
    public void deleteItemFromOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedOrderId = orderService.delItemFromOrder(orderId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/order/"+updatedOrderId);
    }
}

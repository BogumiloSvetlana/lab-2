package server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.domain.Buffet;
import server.service.BuffetService;
import server.service.UserService;
import server.web.model.Requests.BuffetRequest;
import server.web.model.Response.BuffetResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/buffet")
public class BuffetRestController {

    @Autowired
    private BuffetService buffetService;

    @Autowired
    private UserService userService;


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public BuffetResponse getBuffet(@PathVariable Long id) {
        return new BuffetResponse(buffetService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<BuffetResponse> getAll() {
        return buffetService.getAll()
                .stream()
                .map(BuffetResponse::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createBuffet(HttpEntity<BuffetRequest> fullBuffetEntity, HttpServletResponse response) {
        BuffetRequest buffetRequest = fullBuffetEntity.getBody();
        HttpHeaders header = fullBuffetEntity.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[') + 1, authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Buffet buffet = buffetService.save(buffetRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return buffet.getId();
        } else {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Long updateBuffet(@PathVariable Long id, HttpEntity<BuffetRequest> fullBuffetRequest, HttpServletResponse response) {
        BuffetRequest buffetRequest = fullBuffetRequest.getBody();
        HttpHeaders header = fullBuffetRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Buffet buffet = buffetService.update(id,buffetRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return buffet.getId();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteBuffet(@PathVariable Long id, HttpEntity<BuffetRequest> fullBuffetRequest, HttpServletResponse response) {
        HttpHeaders header = fullBuffetRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            buffetService.delete(id);
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Item/{itemId}", method = RequestMethod.POST)
    public void addItemToBuffet(@PathVariable Long buffetId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.addItemToBuffet(buffetId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Item/{itemId}", method = RequestMethod.DELETE)
    public void deleteItemFromBuffet(@PathVariable Long buffetId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.deleteItemFromBuffet(buffetId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Order/{orderId}", method = RequestMethod.POST)
    public void addOrderToBuffet(@PathVariable Long buffetId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.addOrderToBuffet(buffetId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Order/{itemId}", method = RequestMethod.DELETE)
    public void deleteOrderFromBuffet(@PathVariable Long buffetId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.deleteOrderFromBuffet(buffetId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }
}

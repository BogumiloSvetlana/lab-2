package server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import server.domain.User;
import server.service.UserService;
import server.web.model.Requests.UserRequest;
import server.web.model.Response.UserResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public ModelAndView addUserPage() {
        return new ModelAndView("addUserPage");
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public UserResponse getUser(@PathVariable Long id) {
        return new UserResponse(userService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<UserResponse> getAll() {
        return userService.getAll()
                .stream()
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public void createUser(UserRequest userRequest, HttpServletResponse response) {
        User user = userService.save(userRequest);
        response.addHeader(HttpHeaders.LOCATION,"/user/"+user.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public UserResponse updateUser(@PathVariable Long id, @RequestBody UserRequest userRequest) {
        return new UserResponse(userService.update(id,userRequest));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Long id) {
        userService.delete(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{userId}/Order/{orderId}", method = RequestMethod.POST)
    public void addOrderToUser(@PathVariable Long userId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedUserId = userService.addOrderToUser(userId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/user/"+updatedUserId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{userId}/Order/{order}", method = RequestMethod.DELETE)
    public void deleteOrderFromUser(@PathVariable Long userId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedUserId = userService.delOrderFromUser(userId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/user/"+updatedUserId);
    }
}

package server.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.domain.Item;
import server.service.ItemService;
import server.service.UserService;
import server.web.model.Requests.ItemRequest;
import server.web.model.Response.ItemResponse;
import server.web.model.Response.ListOfItemResponces;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/item")
public class ItemRestController {
    @Autowired
    private ItemService itemService;

    @Autowired
    private UserService userService;

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ItemResponse getItem(@PathVariable Integer id) {
        return new ItemResponse(itemService.getById(Integer.toUnsignedLong(id)));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfItemResponces getAll() {
        return new ListOfItemResponces(itemService.getAll().stream().map(ItemResponse::new).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfItemResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        PageRequest pageRequest = new PageRequest(startIndex - 1, finishIndex-startIndex);
        List<ItemResponse> itemResponses = itemService.getAllPagination(pageRequest).stream().map(ItemResponse::new).collect(Collectors.toList());
        return new ListOfItemResponces(itemResponses);
    }


    @RequestMapping(method = RequestMethod.POST)
    public Long createItem(HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        ItemRequest itemRequest = fullItemRequest.getBody();
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Item item = itemService.save(itemRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return item.getId();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Long updateItem(@PathVariable Integer id, HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        ItemRequest itemRequest = fullItemRequest.getBody();
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            Item item = itemService.update(Integer.toUnsignedLong(id),itemRequest);
            response.setStatus(HttpStatus.CREATED.value());
            return item.getId();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id, HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        ItemRequest itemRequest = fullItemRequest.getBody();
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));
        if (userService.checkUpToTimeToken(accesstoken)) {
            itemService.delete(Integer.toUnsignedLong(id));
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
        }
    }
}

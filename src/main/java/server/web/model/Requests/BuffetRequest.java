package server.web.model.Requests;

import com.google.common.base.MoreObjects;


public class BuffetRequest {
    private String name;
    private String location;

    public String getName() {
        return name;
    }

    public BuffetRequest setName(String name) {
        this.name = name;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public BuffetRequest setLocation(String location) {
        this.location = location;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("location", location)
                .toString();
    }
}

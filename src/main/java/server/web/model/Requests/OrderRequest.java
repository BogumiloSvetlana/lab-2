package server.web.model.Requests;

import com.google.common.base.MoreObjects;
import server.domain.Item;

import java.util.List;


public class OrderRequest {
    private Integer totalPrice;
    private List<Item> items;

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public OrderRequest setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public List<Item> getItems() {
        return items;
    }

    public OrderRequest setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .toString();
    }
}

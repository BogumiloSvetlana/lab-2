package server.web.model.Requests;

import com.google.common.base.MoreObjects;


public class ItemRequest {
    private String name;
    private Integer price;

    public String getName() {
        return name;
    }

    public ItemRequest setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public ItemRequest setPrice(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .toString();
    }
}

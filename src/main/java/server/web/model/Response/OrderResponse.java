package server.web.model.Response;

import com.google.common.base.MoreObjects;
import server.domain.Item;
import server.domain.Order;

import java.util.List;


public class OrderResponse {
    private Integer totalPrice;
    private List<ItemResponse> items;
    private BuffetResponse buffet;
    private UserResponse user;

    public OrderResponse(Order order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (Item item: order.getItems()) {
                items.add(new ItemResponse(item));
            }
        if (order.getBuffet() != null)
            this.buffet = new BuffetResponse(order.getBuffet());
        if (order.getUser() != null)
            this.user = new UserResponse(order.getUser());
    }

    public OrderResponse(OrderResponse order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (ItemResponse item: order.getItems()) {
                items.add(new ItemResponse(item));
            }
        if (order.getBuffet() != null)
            this.buffet = new BuffetResponse(order.getBuffet());
        if (order.getUser() != null)
            this.user = new UserResponse(order.getUser());
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public BuffetResponse getBuffet() {
        return buffet;
    }

    public UserResponse getUser() {
        return user;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffet)
                .add("user", user)
                .toString();
    }
}

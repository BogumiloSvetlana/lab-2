package server.web.model.Response;

import java.util.List;


public class ListOfItemResponces {

    private List<ItemResponse> itemResponses;

    public ListOfItemResponces() {
        this.itemResponses = null;
    }

    public ListOfItemResponces(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
    }

    public List<ItemResponse> getItemResponses() {
        return itemResponses;
    }

    public ListOfItemResponces setItemResponses(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
        return this;
    }
}

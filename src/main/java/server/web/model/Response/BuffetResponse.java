package server.web.model.Response;

import com.google.common.base.MoreObjects;
import server.domain.Buffet;
import server.domain.Item;
import server.domain.Order;

import java.util.List;


public class BuffetResponse {
    private String name;
    private String location;
    private List<OrderResponse> orders;
    private List<ItemResponse> items;

    public BuffetResponse(Buffet buffet) {
        this.name = buffet.getName();
        this.location = buffet.getLocation();
        if (!buffet.getOrders().isEmpty())
            for (Order order: buffet.getOrders()) {
                orders.add(new OrderResponse(order));
            }

        if (!buffet.getItems().isEmpty())
            for (Item item: buffet.getItems()) {
                items.add(new ItemResponse(item));
            }

    }


    public BuffetResponse(BuffetResponse buffet) {
        this.name = buffet.getName();
        this.location = buffet.getLocation();
        if (!buffet.getOrders().isEmpty())
            for (OrderResponse order: buffet.getOrders()) {
                orders.add(new OrderResponse(order));
            }

        if (!buffet.getItems().isEmpty())
            for (ItemResponse item: buffet.getItems()) {
                items.add(new ItemResponse(item));
            }
            
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<OrderResponse> getOrders() {
        return orders;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("location", location)
                .add("orders", orders)
                .add("items", items)
                .toString();
    }
}

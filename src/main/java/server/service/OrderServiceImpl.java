package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.domain.Item;
import server.domain.Order;
import server.repository.OrderRepository;
import server.web.model.Requests.OrderRequest;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;



@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private  ItemService itemService;

    @Override
    @Transactional(readOnly = true)
    public Order getById(Long id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }
        return order;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public Order save(OrderRequest orderRequest) {
        Order order = new Order(orderRequest.getTotalPrice(),orderRequest.getItems());
        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Order update(Long id, OrderRequest orderRequest) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        order.setTotalPrice(orderRequest.getTotalPrice() != null ? orderRequest.getTotalPrice() : order.getTotalPrice());
        order.setItems(orderRequest.getItems());

        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        orderRepository.delete(id);
    }

    @Override
    public Long addItemToOrder(Long orderId, Long itemId) {
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        Item item = itemService.getById(itemId);
        order.addItem(item);
        orderRepository.save(order);
        return order.getId();
    }

    @Override
    public Long delItemFromOrder(Long orderId, Long itemId) {
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        Item item = itemService.getById(itemId);
        order.delItem(item);
        orderRepository.save(order);
        return order.getId();
    }

}

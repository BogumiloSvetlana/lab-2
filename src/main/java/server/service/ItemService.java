package server.service;

import org.springframework.data.domain.PageRequest;
import server.domain.Item;
import server.web.model.Requests.ItemRequest;

import java.util.List;


public interface ItemService {

    Item getById(Long id);

    List<Item> getAll();

    List<Item> getAllPagination(PageRequest pageRequest);

    Item save(ItemRequest itemRequest);
    Item save(Item item);

    Item update(Long id, ItemRequest itemRequest);

    void delete(Long id);
}

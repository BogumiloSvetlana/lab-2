package server.service;

import server.domain.Order;
import server.web.model.Requests.OrderRequest;

import java.util.List;


public interface OrderService {

    Order getById(Long id);

    List<Order> getAll();

    Order save(OrderRequest orderRequest);
    Order save(Order order);

    Order update(Long id, OrderRequest orderRequest);

    void delete(Long id);

    Long addItemToOrder(Long orderId, Long itemId);

    Long delItemFromOrder(Long orderId, Long itemId);
}

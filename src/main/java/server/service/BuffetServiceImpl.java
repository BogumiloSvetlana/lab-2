package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.domain.Buffet;
import server.domain.Item;
import server.domain.Order;
import server.repository.BuffetRepository;
import server.web.model.Requests.BuffetRequest;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;


@Service
public class BuffetServiceImpl implements BuffetService {
    @Autowired
    private BuffetRepository buffetRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private OrderService orderService;

    @Override
    @Transactional(readOnly = true)
    public Buffet getById(Long id) {
        Buffet buffet = buffetRepository.findOne(id);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }
        return buffet;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Buffet> getAll() {
        return buffetRepository.findAll();
    }

    @Override
    @Transactional
    public Buffet save(BuffetRequest buffetRequest) {
        Buffet buffet = new Buffet(buffetRequest.getName(), buffetRequest.getLocation());
        return buffetRepository.save(buffet);
    }

    @Override
    @Transactional
    public Buffet update(Long id, BuffetRequest buffetRequest) {
        Buffet buffet = buffetRepository.findOne(id);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.setName(buffetRequest.getName() != null ? buffetRequest.getName() : buffet.getName());
        buffet.setLocation(buffetRequest.getLocation() != null ? buffetRequest.getLocation() : buffet.getLocation());

        return buffetRepository.save(buffet);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        buffetRepository.delete(id);
    }

    @Override
    @Transactional
    public Long addItemToBuffet(Long buffetId, Long itemId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        Item item = itemService.getById(itemId);
        buffet.addItem(item);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long deleteItemFromBuffet(Long buffetId, Long itemId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        Item item = itemService.getById(itemId);
        buffet.delItem(item);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long addOrderToBuffet(Long buffetId, Long orderId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        Order order = orderService.getById(orderId);
        buffet.addOrder(order);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long deleteOrderFromBuffet(Long buffetId, Long orderId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        Order order = orderService.getById(orderId);
        buffet.delOrder(order);
        buffetRepository.save(buffet);
        return buffet.getId();
    }
}
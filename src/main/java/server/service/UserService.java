package server.service;

import server.domain.User;
import server.web.model.Requests.UserRequest;

import java.util.List;


public interface UserService {

    User getById(Long id);

    User findByClientId(String clientId);

    boolean checkUpToTimeToken(String token);

    List<User> getAll();

    User save(UserRequest userRequest);

    User update(Long id, UserRequest userRequest);

    void delete(Long id);

    Long addOrderToUser(Long userId, Long orderId);

    Long delOrderFromUser(Long userId, Long orderId);
}

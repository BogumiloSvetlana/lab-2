package server.service;

import server.domain.Buffet;
import server.web.model.Requests.BuffetRequest;

import java.util.List;


public interface BuffetService {

    Buffet getById(Long id);

    List<Buffet> getAll();

    Buffet save(BuffetRequest buffetRequest);

    Buffet update(Long id, BuffetRequest buffetRequest);

    void delete(Long id);

    Long addItemToBuffet(Long buffetId, Long itemId);

    Long deleteItemFromBuffet(Long buffetId, Long itemId);

    Long addOrderToBuffet(Long buffetId, Long orderId);

    Long deleteOrderFromBuffet(Long buffetId, Long orderId);
}

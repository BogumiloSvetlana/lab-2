package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.domain.Item;
import server.repository.ItemRepository;
import server.web.model.Requests.ItemRequest;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemRepository itemRepository;

    @Override
    @Transactional(readOnly = true)
    public Item getById(Long id) {
        Item item = itemRepository.findOne(id);
        if (item == null) {
            throw new EntityNotFoundException("Item '{" + id + "}' not found");
        }
        return item;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Item> getAllPagination(PageRequest pageRequest) {
        Page<Item> itemPage = itemRepository.findAll(pageRequest);
        return itemPage.getContent();
    }

    @Override
    @Transactional
    public Item save(ItemRequest itemRequest) {
        Item item = new Item(itemRequest.getName(),itemRequest.getPrice());
        return itemRepository.save(item);
    }

    @Override
    @Transactional
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    @Override
    @Transactional
    public Item update(Long id, ItemRequest itemRequest) {
        Item item = itemRepository.findOne(id);
        if (item == null) {
            throw new EntityNotFoundException("Item '{" + id + "}' not found");
        }

        item.setName(itemRequest.getName() != null ? itemRequest.getName() : item.getName());
        item.setPrice(itemRequest.getPrice() != null ? itemRequest.getPrice() : item.getPrice());

        return itemRepository.save(item);
    }

    @Override
    public void delete(Long id) {
        itemRepository.delete(id);
    }
}
